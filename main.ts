import 'dotenv/config'
import express from 'express'
import { PrismaClient } from '@prisma/client'
import endpoints from './api/endpoints'
import { authorizeMiddleware } from './api/authentication'

const app = express()
const prisma = new PrismaClient()

// JSON middleware
app.use(express.json())
app.use(authorizeMiddleware)

endpoints.map((endpoint) => endpoint({ app, prisma }))

app.listen(process.env.APP_PORT, () =>
  console.log(`Server is listening on port ${process.env.APP_PORT}`),
)
