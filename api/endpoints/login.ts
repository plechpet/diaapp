import LoginMddel from '../../model/login'
import { validateRequestModel } from '../request'
import { errorResponse, successResponse } from '../response'
import { EndpointCreator } from '../types'
import { createToken } from '../authentication'
import { createHash, compareHash } from '../hash'
import routes from './config'
import { badCredentials } from '../errors'

const login: EndpointCreator = (params) => {
  const { app, prisma } = params

  app.get(routes.getLoginList.url, async (req, res) => {
    prisma.login.findMany().then(successResponse(res)).catch(errorResponse(res))
  })

  app.post(routes.postCreateLogin.url, async (req, res) => {
    validateRequestModel(req.body, LoginMddel)
      .then((login) => createHash(login.password))
      .then((hash) =>
        prisma.login.create({
          data: {
            ...req.body,
            password: hash,
          },
        }),
      )
      .then(successResponse(res))
      .catch(errorResponse(res))
  })

  app.post(routes.postLogin.url, async (req, res) => {
    try {
      const login = await validateRequestModel(req.body, LoginMddel)
      const user = await prisma.login.findUnique({
        where: { username: login.username },
      })

      if (!user) {
        return errorResponse(res)(badCredentials)
      }

      const authenticated = await compareHash(
        req.body.password,
        user.password || '',
      )

      if (!authenticated) {
        return errorResponse(res)(badCredentials)
      }
      // @TODO dissoc passsword and store better token
      const token = await createToken(user)
      return successResponse(res)({ token })
    } catch (e) {
      errorResponse(res)(e)
    }
  })

  app.get('/me', async (req, res) => {
    return successResponse(res)(res.locals.jwtSession)
  })

  return params
}

export default login
