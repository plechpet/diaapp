type RouteType = {
  method: 'GET' | 'POST' | 'PATCH' | 'DELETE' | 'PUT'
  url: string
  public?: boolean
}

const routes: Record<string, RouteType> = {
  postCreateLogin: {
    method: 'POST',
    url: '/logins',
    public: true,
  },
  postLogin: {
    method: 'POST',
    url: '/login',
    public: true,
  },
  getLoginList: {
    method: 'GET',
    url: '/logins',
  },
}

export default routes
