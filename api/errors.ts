import { Response } from 'express'

type ErrorStatus = { [n: number]: string }

export const notAuthorized: ErrorStatus = { 403: 'Not authorized' }
export const badCredentials: ErrorStatus = { 401: 'Bad credentials' }

export const sendError = (error: ErrorStatus) => (res: Response) => {
  const status = parseInt(Object.keys(error)[0], 10)
  res.status(status)
  res.send(error[status])
}
