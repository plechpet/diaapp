import bcrypt from 'bcrypt'

const saltRounds: number = parseInt(
  process.env.PASSWORD_HASH_SALT_ROUNDS || '8',
  10,
)

export const createHash = (str: string) =>
  bcrypt.genSalt(saltRounds).then((salt) => bcrypt.hash(str, salt))

export const compareHash = (str: string, hash: string) =>
  bcrypt.compare(str, hash)
