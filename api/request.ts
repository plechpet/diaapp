import { LoginType } from '../model/login'
import { Model } from '../model/types'

export const validateRequestModel = (requestObject: LoginType, model: Model) =>
  new Promise<LoginType>((resolve, reject) => {
    Object.keys(model).map((key) => {
      const modelProperty = model[key]
      const requestPropery = requestObject[key as keyof LoginType]

      if (modelProperty.required && !requestPropery) {
        return reject({ error: `Missing parameter ${key}` })
      }

      if (
        modelProperty &&
        requestPropery &&
        typeof requestPropery !== modelProperty.type
      ) {
        return reject({ error: `${key} is not a ${modelProperty.type}` })
      }
    })

    return resolve(requestObject)
  })
