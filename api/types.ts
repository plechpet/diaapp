import { PrismaClient } from '@prisma/client'
import { Express } from 'express'
export type JWTData = {
  username: string
}
export type EndpointCreatorParams = { app: Express; prisma: PrismaClient }
export type EndpointCreator = (
  p: EndpointCreatorParams,
) => EndpointCreatorParams
