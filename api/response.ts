import { Response } from 'express'

export const successResponse = (response: Response) => (toStringify: any) =>
  new Promise((resolve) => {
    resolve(response.send(JSON.stringify(toStringify)))
  })

export const errorResponse = (response: Response) => (toStringify: any) =>
  new Promise((resolve) => {
    resolve(response.send(JSON.stringify(toStringify)))
  })
