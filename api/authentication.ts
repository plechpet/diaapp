import { Request, Response } from 'express'
import { SignJWT, jwtVerify } from 'jose'
import { JWTData } from './types'
import { notAuthorized, sendError } from './errors'
import routes from './endpoints/config'

const secret = new TextEncoder().encode(process.env.JWT_SECRET || '')

export const createToken = (data: JWTData) =>
  new SignJWT(data)
    .setProtectedHeader({ alg: 'HS256' })
    .setIssuedAt()
    .setIssuer(process.env.JWT_ISSUER || '')
    .setAudience(process.env.JWT_AUDIENCE || '')
    .setIssuedAt()
    .setExpirationTime(process.env.JWT_EXPIRATION_TIME || '1 day')
    .sign(secret)

export const verifyToken = (token: string) =>
  jwtVerify(token, secret, {
    issuer: process.env.JWT_ISSUER || '',
    audience: process.env.JWT_AUDIENCE || '',
  })

export const authorizeMiddleware = async (
  req: Request,
  res: Response,
  next: () => void,
) => {
  // @TODO better test if endpoint is public
  const targetRoute = Object.values(routes).find(
    (route) => route.method === req.method && route.url === req.url,
  )

  if (targetRoute?.public) {
    return next()
  }

  const token = req.headers.authorization
  if (!token) {
    return sendError(notAuthorized)(res)
  }

  try {
    const session = await verifyToken(token)
    res.locals.jwtSession = session
    next()
  } catch (e) {
    sendError(notAuthorized)(res)
  }
}
