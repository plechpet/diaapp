# Development

- `yarn install` to install dependencies
- `yarn prisma-migrate-dev` to create dev database
- `yarn dev` to start a development server
