export type ModelProperty = {
  required?: true,
  type: 'string' | 'number',
}

export type Model = Record<string, ModelProperty>
