import { Model } from './types'

export type LoginType = {
  username: string
  password: string
}

const LoginMddel: Model = {
  username: {
    type: 'string',
    required: true,
  },
  password: {
    type: 'string',
    required: true,
  },
}

export default LoginMddel
